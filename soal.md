1. Package yang digunakan untuk menggunakan fitur 'state management' di aplikasi react adalah (yang dipelajari)?

2. function yang digunakan untuk membuat store baru adalah? juga jelaskan parameternya!

3. function yang digunakan untuk menyambung store dengan component React adalah?

4. Component dari "state management" yang digunakan untuk menyediakan/menyambungkan store dengan aplikasi React adalah?

5. Buatlah sebuah aplikasi react menggunakan 'yarn create react-app', gunakan state management yang sudah dipelajari. buatlah state untuk store dengan struktur seperti berikut:
{
  listSantri: ["Alfian","Muamar","Putra"]
}.
dan tampilkan listSantri dalam sebuah component.
gunakan tag "\<ol\>" untuk list, dan beri styling warna untuk text dengan warna "rgb(14, 29, 21)" (tanpa tanda kutip) pada css!
